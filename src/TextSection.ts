import { IResource } from './Resource';
import { IText } from './Text';

export interface ITextSection extends IResource {
    jsonClass: 'TextSection';
    name?: string;
    content?: string;
    components?: IText[];
    relation_to_parent?: 'content' | 'attr';
}
