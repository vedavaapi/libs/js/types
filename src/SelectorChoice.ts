import { ISelector } from './Selector';

export interface ISelectorChoice extends ISelector {
    type?: 'oa:Choice';
    default?: ISelector;
    item?: ISelector;
}
