export interface IPlainObject {
    [x: string]: any;
}
