import { IJsonObject } from './JsonObject';

export type IAction = 'read' | 'updateContent' | 'updateLinks' | 'updatePermissions' | 'createAnnos' | 'createChildren' | 'delete';

export type IResolvedPermissions = IJsonObject & {
    [action in IAction]: boolean;
} & {
    jsonClass: 'ResolvedPermissions';
};
