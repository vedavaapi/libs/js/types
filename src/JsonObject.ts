export type OID = string;

export interface IJsonObject {
    jsonClass: string;
    [prop:string]: any;
}
