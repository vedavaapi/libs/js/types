import { ISelectorChoice } from './SelectorChoice';
import { IFragmentSelector, IBBox, fragmentBBox } from './FragmentSelector';
import { ISvgSelector } from './SvgSelector';

export interface ISvgFragmentSelectorChoice extends ISelectorChoice {
    jsonClass: 'SvgFragmentSelectorChoice';
    default: IFragmentSelector;
    item: ISvgSelector;
}

export function bbox(sel: ISvgFragmentSelectorChoice): IBBox {
    return fragmentBBox(sel.default) as IBBox;
}
