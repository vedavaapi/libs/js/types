import { IJsonObject } from './JsonObject';
import { IDataRepresentation } from './DataRepresentation';
import { IStillImageRepresentation } from './StillImageRepresentation';

export type IRepresentationTypes = 'stillImage' | 'movingImage' | 'sound' | 'dataSet' | 'interactiveResource';

export type IDataRepresentations = IJsonObject & {
    [rt in IRepresentationTypes]?: IDataRepresentation[];
} & {
    jsonClass: 'DataRepresentations';
    default?: IRepresentationTypes;
    stillImage?: IStillImageRepresentation[];
};
