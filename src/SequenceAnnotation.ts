import { IAnnotation } from './Annotation';

export interface ISequenceAnnoBody {
    /** members of the sequence */
    members: Array<{
        /** index of a member */
        index: string | number;
        /** id of member resource */
        resource: string;
    }>;
}

export interface ISequenceAnnotation extends IAnnotation {
    jsonClass: 'SequenceAnnotation';
    body: ISequenceAnnoBody;
    canonical?: string;
}
