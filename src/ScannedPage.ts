import { IResource } from './Resource';
import { ISelector } from './Selector';

export interface IScannedPage extends IResource {
    jsonClass: 'ScannedPage';
    selector?: ISelector;
}
