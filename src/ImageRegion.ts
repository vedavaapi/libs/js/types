import { IResource } from './Resource';
import { IFragmentSelector, IBBox, fragmentBBox } from './FragmentSelector';
import { ISvgFragmentSelectorChoice } from './SvgFragmentSelectorChoice';

export interface IImageRegion extends IResource {
    jsonClass: 'ImageRegion';

    source: string;

    selector: IFragmentSelector | ISvgFragmentSelectorChoice;
}

export function bbox(res: IImageRegion): IBBox {
    return (res.selector.jsonClass === 'FragmentSelector' ? fragmentBBox(res.selector) : fragmentBBox(res.selector.default)) as IBBox;
}
