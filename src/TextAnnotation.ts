import { IAnnotation } from './Annotation';
import { IText } from './Text';

export interface ITextAnnotation extends IAnnotation {
    jsonClass: 'TextAnnotation';
    body?: IText[];
}
