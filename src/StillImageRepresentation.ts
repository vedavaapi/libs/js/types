import { IDataRepresentation } from './DataRepresentation';
import { purgeUndefined } from './utils';

export interface IStillImageRepresentation extends IDataRepresentation {
    jsonClass: 'StillImageRepresentation';
    implements?: string[];
}

export function from({ ooldId, implementedInterfaces }: { ooldId: string, implementedInterfaces?: string[] }): IStillImageRepresentation {
    const doc = {
        jsonClass: 'StillImageRepresentation',
        data: `_OOLD:${ooldId}`,
        implements: implementedInterfaces,
    };
    purgeUndefined(doc);
    return doc as IStillImageRepresentation;
}

export function supportsIIIF(repr: IStillImageRepresentation): boolean {
    return repr.implements !== undefined && Array.isArray(repr.implements) && repr.implements.includes('iiif_image');
}
