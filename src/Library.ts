import { IJsonObject } from './JsonObject';
import { IStillImageRepresentation } from './StillImageRepresentation';

export interface ILibrary extends IJsonObject {
    jsonClass: 'Library';
    name: string;
    description?: string;
    logo?: IStillImageRepresentation;
}
