import { IResource } from './Resource';

export interface IService extends IResource {
    jsonClass: 'Service';
    name: string;
    url: string;
    handledTypes: string[];
    apis?: string[];
}
