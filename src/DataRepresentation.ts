import { IJsonObject } from './JsonObject';

export interface IDataRepresentation extends IJsonObject {
    data: string;
}

export function ooldId(repr: IDataRepresentation): string | null {
    return repr.data ? repr.data.slice(6) : null;
}
