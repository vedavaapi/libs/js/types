import { IJsonObject } from './JsonObject';

export interface IMetadataItem extends IJsonObject {
    jsonClass: 'MetadataItem',
    label: string;
    value: string;
}
