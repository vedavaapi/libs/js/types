import { IJsonObject, OID } from './JsonObject';
import { IMetadataItem } from './MetadataItem';
import { IDataRepresentations } from './DataRepresentations';

export interface IResource extends IJsonObject {
    metadata?: Array<IMetadataItem>;
    /** _id(s) of source resources */
    source?: OID | OID[];
/** _id(s) of target resources */
    target?: OID | OID[];
    body?: any;
    selector?: any;
    representations?: IDataRepresentations;
    jsonClassLabel?: string;
    /** unique _id of resource */
    _id?: OID;
    creator?: OID;
    _reached_ids?: { [field: string]: OID[] };
    [x: string]: any;
}

export function metaMap(res: IResource): any {
    if (res.metadata === undefined || !Array.isArray(res.metadata)) {
        return {};
    }
    const meta: any = {};
    res.metadata.forEach((item) => {
        meta[item.label] = item.value;
    });
    return meta;
}
