import { IResource } from './Resource';
import { OID } from './JsonObject';

export interface IAnnotation extends IResource {
    type: 'oa:Annotation';
    target: OID | OID[];
    source?: undefined;
    body?: any;
}
