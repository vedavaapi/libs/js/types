import { IJsonObject } from './JsonObject';

export interface ISelector extends IJsonObject {
    refinedBy?: ISelector;
}
