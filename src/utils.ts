
/**
 * purges any undefined attrs.
 * @param doc - source doc
 */
// eslint-disable-next-line import/prefer-default-export
export function purgeUndefined(doc: any): void {
    Object.keys(doc).forEach((k) => {
        if (doc[k] === undefined) {
            // eslint-disable-next-line no-param-reassign
            delete doc[k];
        }
    });
}


export function truncate(str: string, n: number, useWordBoundary?: boolean) {
    if (str.length <= n) { return str; }
    const subString = str.substr(0, n - 1);
    return `${useWordBoundary
        ? subString.substr(0, subString.lastIndexOf(' '))
        : subString}…`;
}
