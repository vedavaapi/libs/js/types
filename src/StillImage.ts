import { IResource } from './Resource';

export interface IStillImage extends IResource {
    jsonClass: 'StillImage';
    namespace?: string;
    identifier?: string;
    url?: string;
}
