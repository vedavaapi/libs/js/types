import { IResource } from './Resource';
import { IText } from './Text';

export interface ITextDocument extends IResource {
    jsonClass: 'TextDocument';
    author?: string[];
    content?: string;
    components?: IText[];
}
