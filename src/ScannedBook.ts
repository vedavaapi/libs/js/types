import { IResource } from './Resource';
import { IStillImageRepresentation } from './StillImageRepresentation';

export interface IScannedBook extends IResource {
    jsonClass: 'ScannedBook';
    title: string;
    author?: string[];
    cover?: IStillImageRepresentation;
}

export function imgRepr(res: IScannedBook) {
    return res.cover ? { repr: res.cover } : null;
}
