import { ISelector } from './Selector';

export interface IFragmentSelector extends ISelector {
    jsonClass: 'FragmentSelector';
    type?: 'oa:FragmentSelector';
    value: string;
    confirmsTo?: string;
}

export interface IBBox {
    x: number;
    y: number;
    w: number;
    h: number;
}

export const fragmentBBoxRegex = new RegExp('^xywh=([0-9-]+),([0-9-]+),([0-9-]+),([0-9-]+)$');


export function fragmentBBox(sel: IFragmentSelector): IBBox | null {
    const match = sel.value.match(fragmentBBoxRegex);
    if (!match) {
        return null;
    }
    const [, x, y, w, h] = match;
    return { x: Number(x), y: Number(y), w: Number(w), h: Number(h) };
}
