import { ISelector } from './Selector';

export interface ISvgSelector extends ISelector {
    jsonClass: 'SvgSelector';
    /** svg as string */
    value: string;
    type?: 'oa:SvgSelector';
}
