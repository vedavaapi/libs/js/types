import { IResource } from './Resource';
import { purgeUndefined } from './utils';

export interface IText extends IResource {
    jsonClass: 'Text';
    chars: string;
    language?: string;
    script?: string;
}

export function from({ chars, language, script }: { chars: string, language?: string, script?: string }): IText {
    const doc = {
        jsonClass: 'Text',
        chars,
        language,
        script,
    };
    purgeUndefined(doc);
    return doc as IText;
}
